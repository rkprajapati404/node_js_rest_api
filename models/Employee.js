const mongoose = require("mongoose");

const employeeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is a required field'],
        maxlength: [100, 'Max 100 characters are allowed'],
    },
    age: {
        type: Number,
        min: [18, 'Age must be at least 18'],
        max: [65, 'Age must be at most 65'],
    },
    position: {
        type: String
    },
    department: {
        type: String,
        required: [true, 'Department is required'],
        minlength: [2, 'Min 2 characters are allowed'],
        maxlength: [10, 'Max 10 characters are allowed'],
    }
});

module.exports = mongoose.model("Employee", employeeSchema);
