const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Title is a required field'],
        maxlength: [100, 'Title can be at most 100 characters long'],
    },
    description: {
        type: String,
        maxlength: [500, 'Description can be at most 500 characters long'],
    },
    duration: {
        type: Number,
        required: [true, 'Duration is a required field'],
        min: [1, 'Duration must be at least 1 hour'],
    },
    level: {
        type: String,
        enum: ['Beginner', 'Intermediate', 'Advanced'],
        required: [true, 'Level is a required field'],
    },
    publishedDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Course', courseSchema);
