const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const bcrypt = require('bcrypt');


router.post("/signup", async (req, res) => {
    try {

        let body = req.body;

        const salt = await bcrypt.genSalt(10);

        let incryptedPass = await bcrypt.hash(body.password, salt);
        body.password = incryptedPass;

        const user = await User.create(body);

        res.status(200).send({
            message: "Username : " + user.username + " successfiully created"
        });

    } catch (error) {
        if (error.code === 11000) {
            const field = Object.keys(error.keyValue)[0];
            res.status(400).json({ message: `Duplicate key error: ${field} already exists` });
        } else {
            res.status(400).json({ message: `Error : ${error} ` });
        }

    }
});


router.post("/login", async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username });
        if (!user) {
            return res.status(200).json({ message: "Invalid Credentials" });
        }
        const isMatch = await bcrypt.compare(password, user.password);

        if (isMatch) {
            return res.status(200).json({ success: true, message: "Login successful" });
        } else {
            return res.status(200).json({ message: "Invalid Credentials" });
        }

    } catch (error) {
        return res.status(400).json({ message: `Error : ${error}` });
    }
});


module.exports = router;
