const express = require("express");
const router = express.Router();
const Employee = require("../models/Employee.js");

router.get("/", async (req, res) => {
    try {
        let employees = await Employee.find();
        res.status(200).send({
            message: employees
        });
    } catch (error) {
        console.error('Error fetching data:', error);
        res.status(400).send('Validation Error');
    }
});

router.post("/", async (req, res) => {
    try {
        const employee = await Employee.create(req.body)
        res.status(200).send({
            message: employee
        });
    } catch (error) {
        res.status(400).send({
            message: "" + error
        });
    }
});


router.put("/:id", async (req, res) => {
    try {
        const { id } = req.params;
        const updatedEmployee = await Employee.findByIdAndUpdate(id, req.body, { new: true, runValidators: true });
        res.status(200).send({
            message: updatedEmployee
        });
    } catch (error) {
        res.status(400).send({
            message: "" + error
        });
    }
});

router.delete("/:id", async (req, res) => {
    try {
        const { id } = req.params;
        let updatedEmployee = await Employee.findByIdAndDelete(id);
        if (updatedEmployee === null) {
            updatedEmployee = "EMployee Not Found By Id " + id
        }
        res.status(200).send({
            message: updatedEmployee
        });
    } catch (error) {
        console.error('Error fetching data:', error);
        res.status(500).send('Internal Server Error');
    }
});

module.exports = router;
