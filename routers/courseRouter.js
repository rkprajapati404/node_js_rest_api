const express = require("express");
const router = express.Router();
const Course = require("../models/Course.js");

router.get("/", async (req, res) => {
    try {
        let courses = await Course.find();
        console.log("Fetched : ");
        res.status(200).send({
            message: courses
        });
    } catch (error) {
        console.error('Error fetching data:', error);
        res.status(400).send('Validation Error');
    }

});

router.post("/", async (req, res) => {
    try {
        const course = await Course.create(req.body)
        res.status(200).send({
            message: course
        });
    } catch (error) {
        res.status(400).send({
            message: "" + error
        });
    }
});





module.exports = router;
