var express = require('express');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');
const userRouter = require('./routers/userRouter');
const requestLogger = require('./middleware/RequestLogger');

var app = express();

app.use(express.json({
    extended: true
}));

dotenv.config({
    path: './config/dev.env'
});

console.log('Environment set to:', process.env.NODE_ENV);

mongoose.connect(process.env.DATABASE_URL, { useUnifiedTopology: true, useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', (error) => console.log(error.red));
db.once('open', () => console.log("Connected to Database".green.underline.bold));

//API's
app.get("/health", (req, res) => {
    res.send("API Running");
});

app.use(requestLogger);

app.use('/api/v1/employee', require('./routers/employeeRouter'));

app.use('/api/v1/course', require('./routers/courseRouter'));

app.use('/api/v1/user', userRouter);


let server = app.listen(process.env.PORT, () => {
    console.log(`Server is running on http://localhost:${process.env.PORT}/`);
});
